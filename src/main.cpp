/*****************************************************************************
 * %{Cpp:License:FileName}
 *
 * Created: 7 2018 by amir
 *
 * Copyright 2018 "INTERSET". All rights reserved.
**************************************************************************/
#include <QCoreApplication>

#include "keygen.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    KeyGen::sdelatHorosho();
    return a.exec();
}
