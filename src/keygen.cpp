/*****************************************************************************
 * keygen.cpp
 *
 * Created: 7 2018 by amir
 *
 * Copyright 2018 "INTERSET". All rights reserved.
**************************************************************************/
#include "keygen.h"

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/sha.h>
#include <openssl/ripemd.h>
#include <openssl/bn.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <iomanip>

KeyGen::KeyGen() {

}

KeyGen::~KeyGen() {

}

void KeyGen::sdelatHorosho() {
    const int nid = NID_X9_62_prime256v1;

    char* privFileName = "test.pem";
    char* pubFileName = "test.pub";

    BIO *bio_err = BIO_new(BIO_s_file());
    bio_err = BIO_new_fp(stdout, BIO_NOCLOSE);

    BIO *bioPriv = BIO_new(BIO_s_file());
    bioPriv = BIO_new_file(privFileName, "wb");

    BIO *bioPub = BIO_new(BIO_s_file());
    bioPub = BIO_new_file(pubFileName, "wb");

    EC_GROUP *group = EC_GROUP_new_by_curve_name(nid);


    EC_KEY *eckey = EC_KEY_new();
    if (eckey == NULL) {
        BIO_printf(bio_err, "Error in key creating");
    }

    if (EC_KEY_set_group(eckey, group) == 0) {
        BIO_printf(bio_err, "Unable to set group when generating key\n");
        EC_KEY_free(eckey);

    }

    if (!EC_KEY_generate_key(eckey)) {
        BIO_printf(bio_err, "unable to generate key\n");
        EC_KEY_free(eckey);

    }

    if(!PEM_write_bio_ECPrivateKey(bioPriv, eckey, NULL, NULL, 0, 0, NULL))
        BIO_printf(bio_err, "Error writing private key data in PEM format");
    if(!PEM_write_bio_EC_PUBKEY(bioPub, eckey))
        BIO_printf(bio_err, "Error writing public key data in PEM format");

    BIO_printf(bio_err, "Keys successfully generated!\n");

    unsigned char pub_key_buf[65];
    EC_POINT *pub_point = const_cast<EC_POINT*>(EC_KEY_get0_public_key(eckey));
    BN_CTX* bnctx = BN_CTX_new();

    EC_POINT_point2oct(group, pub_point, POINT_CONVERSION_UNCOMPRESSED, pub_key_buf, 33, bnctx);

    BIGNUM *x = BN_new();
    BIGNUM *y = BN_new();
    EC_POINT_get_affine_coordinates_GFp(group, pub_point, x, y, bnctx);

    unsigned char x_char[32];
    unsigned char y_char[32];
    BN_bn2bin(x, x_char);
    BN_bn2bin(y, y_char);

    std::string pub_string;
    char start = 0x04;
    pub_string.append("0");
    pub_string[0] = start;
    pub_string.append(std::begin(x_char), std::begin(x_char) + 32);
    pub_string.append(std::begin(y_char), std::begin(y_char) + 32);

    unsigned char pub_key[65];
    std::copy(pub_string.begin(), pub_string.end(), pub_key);
    std::cout << "\nStep 1. pub key\n" << arr2HexString(pub_key, 65) << std::endl;

    unsigned char md[32];
    SHA256(pub_key, 65, md);
    std::cout << "\nStep 2. SHA-256\n" << arr2HexString(md, 32) << std::endl;

    unsigned char rmd_buf[32];
    unsigned char *rmd = RIPEMD160(md, sizeof(md), rmd_buf);
    std::string rmd_string;
    char xz = 0x0;
    rmd_string.append("0");
    rmd_string[0] = xz;
    rmd_string.append(rmd, rmd + 20);
    std::copy(rmd_string.begin(), rmd_string.begin() + 21, rmd);
    std::cout << "\nStep 3. 0x0 + PIPEMD-160\n" << arr2HexString(rmd_buf, 21) << std::endl;

    unsigned char md2[32];
    SHA256(rmd, 21, md2);
    std::cout << "\nStep 4. SHA-256\n" << arr2HexString(md2, 32) << std::endl;

    unsigned char md3[32];
    SHA256(md2, 32, md3);
    std::cout << "\nStep 5. 4 bytes\n" << arr2HexString(md3, 4)<<  std::endl;

    std::string result;
    result.append("0x");
    result.append(arr2HexString(rmd_buf, 21));
    result.append(arr2HexString(md3, 4));
    std::cout << "\nStep 6. Address\n" << result <<  std::endl;

    EC_KEY_free(eckey);
    EC_GROUP_free(group);
    BIO_free_all(bio_err);
    BIO_free_all(bioPriv);
    BIO_free_all(bioPub);
    BN_free(x);
    BN_free(y);
    BN_CTX_free(bnctx);
}


std::string arr2HexString(const unsigned char charbuf[], size_t size) {
    std::stringstream priv_ss;
    priv_ss << std::hex << std::setfill('0');
    for (size_t i = 0; i < size; ++i) {
        priv_ss << std::setw(2) << static_cast<unsigned>(charbuf[i]);
    }
    return priv_ss.str();
}
